package sandwich;

public class BLTSandwich implements ISandwich{
    private String filling;

    public BLTSandwich(){
        this.filling= "Beacon, lettuce, tomato";
    }

    public void addFilling (String topping){
        this.filling += topping;
    }
    
    public boolean isVegetarian(){
        return false;
    }

    public String getFilling(){
        return this.filling;
    }
}
