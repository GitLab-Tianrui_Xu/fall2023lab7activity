package sandwich;

public class CaeserSandwich extends VegetarianSandwich {
    private String filling;
    public CaeserSandwich() {
        this.filling = "Caeser dressing";
    }

    public String getFilling() {
        return this.filling;
    }
    
    @Override
    public String getProtein() {
        return "Anchovies";
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }
}