package sandwich;

public abstract class VegetarianSandwich implements ISandwich{
    private String filling;
    final String[] toppingException = {"chicken", "beef", "fish", "meat", "pork"};
    public VegetarianSandwich() {
        this.filling = "";
    }

    // public String getFilling() {
    //     return this.filling;
    // }

    public void addFilling(String topping) {
        for(int i = 0; i < this.toppingException.length; i++) {
            if(topping.contains(this.toppingException[i])) {
                throw new IllegalArgumentException();
            }
        }
        this.filling += (", " + topping.toLowerCase());
    }

    final public boolean isVegetarian() {
        return true;
    }

    public boolean isVegan() {
        if(this.filling.contains("cheese") || this.filling.contains("egg")) {
            return false;
        }
        return true;
    }

    abstract String getProtein();
}
