package sandwich;

public class TofuSandwich extends VegetarianSandwich{
    private String filling;
    public TofuSandwich(){
        this.filling="tofu";
    }
    @Override
    public String getProtein(){
        return "tofu";
    }

    public String getFilling(){
        return this.filling;
    }
}
