package sandwich;
import org.junit.Test;
import static org.junit.Assert.*;
public class BLTSandwichTest {
    @Test
    public void constructorTest() {
        BLTSandwich blt = new BLTSandwich();
        assertEquals("Beacon, lettuce, tomato", blt.getFilling());
    }

    @Test
    public void addFillingTest() {
        BLTSandwich blt = new BLTSandwich();
        blt.addFilling("filling");
        assertEquals(true, blt.getFilling().contains("filling"));
  
    }

    @Test
    public void getFillingTest() {
        BLTSandwich blt = new BLTSandwich();
        assertEquals(false, blt.isVegetarian());
    }
}
