package sandwich;
import org.junit.Test;
import static org.junit.Assert.*;

public class VsTest {

    @Test
    public void ConstructorTest(){
        VegetarianSandwich vs = new VegetarianSandwich();
        assertEquals("", vs.getFilling());
    }
    @Test
    public void addfillingTest(){
        VegetarianSandwich vs = new VegetarianSandwich();
        vs.addFilling("lol");
        assertEquals(true, vs.getFilling().contains("lol"));

    }
    @Test
    public void isvegieTest(){
        VegetarianSandwich vs = new VegetarianSandwich();
        assertEquals(true,vs.isVegetarian());
    }

    @Test
    public void isVeganFalse() {
        VegetarianSandwich vs = new VegetarianSandwich();
        vs.addFilling("cheese");
        assertEquals(false,vs.isVegan());
    }

    @Test
    public void isVeganTru() {
        VegetarianSandwich vs = new VegetarianSandwich();
        assertEquals(true,vs.isVegan());
    }
    
}
